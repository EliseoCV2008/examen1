
// Online C++ Compiler - Build, Compile and Run your C++ programs online in your favorite browser

#include<iostream>
#include<cmath>
using namespace std;
// Pregunta 1: Complete la declaración de la clase base Figura
class Figura {
public:
virtual double calcularArea() = 0;
};
// Pregunta 2: Complete la declaración de la clase Triangulo que hereda de Figura
class Triangulo : public Figura {
private:
// Atributos
double lado1;
double lado2;
double lado3;
public:
// Constructor
// Método para calcular el área
Triangulo(double lado1, double lado2, double lado3) : lado1(lado1), lado2(lado2), lado3(lado3) {}
double calcularArea() override {
    //usamos la formula de Heron para calcular el área de un Triangulo
    double s= (lado1 + lado2 + lado3) / 2;
    double area = sqrt(s * (s-lado1)* (s - lado2) * (s -lado3));
    return area;
}
};
// Pregunta 3: Complete la declaración de la clase Cuadrado que hereda de Figura
class Cuadrado : public Figura {
private:
// Atributo
double lado;
public:
// Constructor
// Método para calcular el área
Cuadrado(double lado) : lado(lado) {}
double calcularArea() override {
    return lado * lado; //area del cuadrado = lado * lado
}
};
// Pregunta 4: Complete el código en la función main
int main() {
// Crear instancias de Triangulo y Cuadrado
// Mostrar el cálculo del área de cada uno
Triangulo triangulo(3.0, 4.0, 5.0);  // Ejemplo de un triángulo con lados de longitud 3, 4 y 5.
    Cuadrado cuadrado(5.0);             // Ejemplo de un cuadrado con un lado de longitud 5.

    double areaTriangulo = triangulo.calcularArea();
    double areaCuadrado = cuadrado.calcularArea();

    std::cout << "Area del triangulo: " << areaTriangulo << std::endl;
    std::cout << "Area del cuadrado: " << areaCuadrado << std::endl;


    return 0;
}

//Explique brevemente en qu� consiste la herencia en C++ y cu�les son sus beneficios en la programaci�n orientada a objetos.
//La herencia en C++ es un mecanismo que permite a una clase heredar propiedades y comportamientos de otra clase, llamada clase base o superclase,
//La clase que hereda se llama clase derivada o subclase La herencia es uno de los conceptos fundamentales de la programaci�n orientada a objetos y permite la reutilizaci�n de variables y funcionalidades que ya han sido definidas en una clase base
//Beneficios:
//Reutilizaci�n de c�digo: Una clase derivada puede heredar comportamiento de una clase base, por lo que el c�digo no necesita volver a ser escrito para la clase derivada
//Jerarqu�a de clases: La herencia permite crear jerarqu�as de clases, donde una clase derivada puede heredar de una clase base, y a su vez, otra clase puede heredar de la clase derivada
